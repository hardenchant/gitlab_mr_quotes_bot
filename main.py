import logging
import os
import urllib.parse
from datetime import datetime

import requests

logging.basicConfig(
    level=logging.INFO,
    format='[%(asctime)s] %(levelname)s [%(funcName)s:%(lineno)d] %(message)s'
)


class GitLabAPIException(Exception):
    pass


class GitLabAPI:
    def __init__(self, token, api_base_url='https://gitlab.com/api/v4/', **kwargs):
        """
        :param token: gitlab token
        :param api_base_url: gitlab api endpoint url
        """
        self.default_request_kwargs = {
            'headers': {
                'Authorization': f'Bearer {token}'
            }
        }
        self.default_request_kwargs.update(kwargs)
        self.api_base_url = api_base_url
        self.session = requests.Session()

    def _request_json(self, url_path, method='get', *args, **kwargs):
        """
        Performs request with required headers and returns json if ok
        :param url_path: path to endpoint with cutted api part - https://gitlab.com/api/v4/
        :param method: get/post/...
        :param args:
        :param kwargs:
        :return:
        """
        request_kwargs = dict()
        request_kwargs.update(self.default_request_kwargs)
        request_kwargs.update(kwargs)
        response = self.session.request(
            method,
            urllib.parse.urljoin(self.api_base_url, url_path),
            *args,
            **request_kwargs
        )
        if not response.ok:
            raise GitLabAPIException(f'Response status is not ok: {response.status_code}')
        return response.json()

    def get_project_mrs(self, project_id, **params):
        return self._request_json(f'projects/{project_id}/merge_requests', params=params)

    def update_mr_description(self, project_id: int, mr_iid: int, **data):
        return self._request_json(f'projects/{project_id}/merge_requests/{mr_iid}', method='put', json=data)

    def upload_file_to_project(self, project_id: int, file):
        return self._request_json(f'projects/{project_id}/uploads', method='post', files={'file': file})

    def get_branches_for_commit(self, project_id: int, commit_hash: str):
        return [b['name'] for b in self._request_json(f'projects/{project_id}/repository/commits/{commit_hash}/refs',
                                                      params={'type': 'branch'})]


class MergeRequestUpdater:
    def __init__(self, gitlab_api: GitLabAPI, gitlab_project_id):
        """
        :param gitlab_api: gitlab api which will be used for update
        :param gitlab_project_id: project to update mrs from
        """
        self.gitlab_api = gitlab_api
        self.gitlab_project_id = gitlab_project_id

    def get_mr_description_merged_mr_list(self, mr_data, level=0, max_level=0, only_after=None) -> list:
        """
        Recursively get mrs tree (check only 20 last created mrs)
        :param mr_data: mr dict
        :param level: current nesting level
        :param max_level: max nesting level
        :param only_after: mrs before this datetime will be dropped
        :return: description strings list
        """
        if level > max_level:
            return []

        merged_mrs = self.gitlab_api.get_project_mrs(
            self.gitlab_project_id,
            state='merged',
            target_branch=mr_data['source_branch'],
            order_by='created_at',
            sort='desc'
        )
        mr_list = []
        for merged_mr in merged_mrs:
            if only_after:
                mr_update_time = datetime.strptime(merged_mr['updated_at'][:19], '%Y-%m-%dT%H:%M:%S')
                if mr_update_time <= only_after:
                    continue
            if self.is_mr_already_merged_in_branch(merged_mr['merge_commit_sha'], mr_data['target_branch']):
                continue

            mr_list.append(f"{level * '  '}- !{merged_mr['iid']} **{merged_mr['title']}**")
            mr_list.extend(self.get_mr_description_merged_mr_list(merged_mr, level=level + 1, only_after=only_after))
        return mr_list

    def is_mr_already_merged_in_branch(self, mr_hash: str, target_branch: str) -> bool:
        return target_branch in self.gitlab_api.get_branches_for_commit(self.gitlab_project_id, mr_hash)

    def is_update_required(self, mr_data: dict) -> bool:
        """
        Check if mr updates required
        :param mr_data: mr dicr
        :return:
        """
        for line in mr_data['description'].split('\n'):
            stripped = line.strip()
            if stripped and stripped[0] == '>' and '(c)' in line:
                return False
        return True

    def get_mr_quote(self) -> str:  # noqa
        """
        Get quote to insert
        :return:
        """
        quote = 'Делайте безумные поступки, жизнь одна.'
        author = 'Джейсон Стейтем'
        return f'> {quote} (c) {author}'

    def get_upload_file(self) -> bytes:  # noqa
        """
        Get file bytes content which need to upload for quote
        :return:
        """
        with open('giphy.gif', 'rb') as f:
            return f.read()

    def get_mr_description_appendix(self, mr_data: dict, upload: dict = None) -> str:
        """
        Returns new mr description appendix with quote
        :param mr_data: mr dict
        :param upload: file dict which will be attached to bottom of quote or None
        :return: description appendix string
        """
        appendix = []
        merged_mr_list = self.get_mr_description_merged_mr_list(mr_data)
        if merged_mr_list:
            appendix.extend([
                'Текущий МР включает в себя другие МРы:',
                *merged_mr_list,
                '\n'
            ])
        appendix.append(self.get_mr_quote())
        if upload:
            appendix.append(f"\n> !{upload['markdown']}")
        return '\n'.join(appendix)

    def update_opened_mrs(self):
        """
        Check if mrs not updated and add quote and image
        :return:
        """
        opened_mrs = g_api.get_project_mrs(self.gitlab_project_id, state='opened')
        for opened_mr in opened_mrs:
            if not self.is_update_required(opened_mr):
                continue
            upload = g_api.upload_file_to_project(self.gitlab_project_id, self.get_upload_file())
            g_api.update_mr_description(
                self.gitlab_project_id,
                opened_mr['iid'],
                description=opened_mr['description'] + '\n\n\n' + self.get_mr_description_appendix(opened_mr, upload),
            )
            logging.info(f"Updated mr description {opened_mr['source_branch']} -> {opened_mr['target_branch']}")


if __name__ == '__main__':
    GITLAB_API_BASE_URL = os.getenv('GITLAB_API_BASE_URL')
    GITLAB_API_TOKEN = os.getenv('GITLAB_API_TOKEN')
    GITLAB_PROJECT_ID = os.getenv('GITLAB_PROJECT_ID')

    g_api = GitLabAPI(GITLAB_API_TOKEN, api_base_url=GITLAB_API_BASE_URL, verify=False)
    MergeRequestUpdater(g_api, GITLAB_PROJECT_ID).update_opened_mrs()
