## GitLab Statham quotes bot

Bot adds quotes, gifs and collect merged to source branch merge requests to merge request description.


Roadmap:
- [ ] update merged branch list (now only create lists and quotes for mrs with blank description)
- [ ] check more than 20 mrs (pagination support required)
- [ ] smart generation of Statham quotes
- [ ] random funny gifs from internet
- [ ] runs periodically (? crontab for assholes ?)
